#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {

    int ladoA = 0, ladoB = 0, ladoC = 0;
    printf("Informe o lado A: ");
    scanf("%d", &ladoA);

    printf("Informe o lado B: ");
    scanf("%d", &ladoB);

    printf("Informe o lado C: ");
    scanf("%d", &ladoC);

    if(ladoA > (ladoB + ladoC) || ladoB > (ladoA + ladoC) || ladoC > (ladoA + ladoB)) {
        printf("Os valores nao formam um triangulo\n");
        return;
    }

    if(ladoA == ladoB && ladoB == ladoC) {
        printf("Eh um triangulo equilatero\n");
    } else if (ladoA == ladoB || ladoB == ladoC || ladoC == ladoA) {
        printf("Eh um triangulo isosceles\n");
    } else {
        printf("Eh um triangulo escaleno\n");
    }
}